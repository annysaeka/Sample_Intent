package id.co.sample_intent;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

public class MainActivity extends AppCompatActivity {

    private Button btnMod1,btnMod2 ;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        btnMod1 = (Button)findViewById(R.id.btnMod1);
        btnMod2 = (Button)findViewById(R.id.btnMod2);

        btnMod1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(MainActivity.this, Mod1Activity.class);
                startActivity(intent);
            }
        });

        btnMod2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(MainActivity.this, Mod2Activity.class);
                startActivity(intent);
            }
        });
    }
}
